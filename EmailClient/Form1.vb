Imports System.Net.Mail
Public Class Form1
    Public hostname, stremail, strepass, port, strsendto, subject, strtextbody, DatabasePath As String
    Public HaveAttachment As Boolean
    Public att As System.Net.Mail.Attachment
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            DoLog("Starting Program")
            DoLog("Saving Path")
            DoLog("Command: " & Command())
            SaveSetting("NCSEmailClient", "DATA", "Location", Application.ExecutablePath.ToString)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''Check Registry
            DoLog("Checking Registry-License")
            If GetSetting("Filestype", "Filestype1", "f1", 0) = "0" Then
                MsgBox("Please use this utility with NCS Product. Code No-L")
                DoLog("Ending Program")
                Application.Exit()
            End If

            DoLog("Checking Registry-Commands")
            If My.Application.CommandLineArgs.Count = 0 Then
                MsgBox("Please use this utility with NCS Product. Code No-C")
                DoLog("Ending Program")
                Application.Exit()
            End If

            DoLog("Opening DB")
            Call OpenDB(ExtractCommand("-db"))
            DoLog("Database " & ExtractCommand("-db"))

            DoLog("Setting Server for " & stremail.Substring(InStr(stremail, "@")))
            Call SetServer(stremail.Substring(InStr(stremail, "@")))

            Me.Timer1.Interval = 2000
            Me.Timer1.Enabled = True
            Me.Timer1.Start()
        Catch ex As Exception
            MsgBox("FL: " & ex.Message)
            DoLog("FL: " & ex.Message)
            DoLog("Ending Program")
            Application.Exit()
        End Try
        
    End Sub
    Function ExtractCommand(ByVal Param As String) As String
        Try
            Dim i As Integer
            Do Until i = (My.Application.CommandLineArgs.Count - 1)
                If Param = My.Application.CommandLineArgs.Item(i) Then
                    Return My.Application.CommandLineArgs.Item(i + 1)
                End If
                i = i + 1
            Loop

        Catch ex As Exception
            MsgBox("EC: " & ex.Message)
            DoLog("EC: " & ex.Message)
        End Try
        
    End Function
    Function OpenDB(ByVal DatabaseLocation As String)
        Try

            Dim cn As New ADODB.Connection()
            Dim rs As New ADODB.Recordset()

            If My.Computer.FileSystem.FileExists(DatabaseLocation) = False Then
                MsgBox("Please use this utility with NCS Product. Code No-D")
                Application.Exit()
            End If
            DatabasePath = DatabaseLocation
            cn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & DatabaseLocation & ";"
            'cn.Mode = ADODB.ConnectModeEnum.adModeShareDenyNone
            cn.Open()
            rs.Open("email_setting", cn)
            stremail = rs.Fields.Item(1).Value.ToString
            strepass = rs.Fields.Item(2).Value.ToString

            rs.Close()
            rs.ActiveConnection = Nothing
            cn.Close()


        Catch ex As Exception
            MsgBox("ODB: " & ex.Message)
            DoLog("ODB: " & ex.Message)
            DoLog("Ending Program")
            Application.Exit()

        End Try

        
    End Function
    Function SetServer(ByVal MailISP As String)
        Try

            If My.Computer.FileSystem.FileExists(MailISP & ".xml") Then
            Else
                My.Computer.Network.DownloadFile("https://autoconfig.thunderbird.net/v1.1/" & MailISP, MailISP & ".xml")
            End If

            Dim reader As System.Xml.XmlTextReader = New System.Xml.XmlTextReader(MailISP & ".xml")
            Dim e_mail As New System.Net.Mail.MailMessage()
            Dim ele, val As String
            Dim issmtp As Boolean


            Do While (reader.Read())
                Select Case reader.NodeType
                    Case System.Xml.XmlNodeType.Element 'Display beginning of element.
                        ele = reader.Name
                        val = ""
                        If reader.Name = "outgoingServer" Then
                            issmtp = True
                        End If

                    Case System.Xml.XmlNodeType.Text 'Display the text in each element.
                        val = reader.Value
                        If issmtp = True Then
                            Select Case ele
                                Case "hostname"
                                    hostname = val
                                Case "port"
                                    port = val
                                Case "socketType"
                                    'If val = "SSL" Then Smtp_Server.EnableSsl = True
                            End Select
                        End If

                    Case System.Xml.XmlNodeType.EndElement 'Display end of element.
                        If issmtp = True And reader.Name = "outgoingServer" Then
                            Exit Do
                        End If

                End Select
            Loop
            reader.Close()


        Catch ex As Exception
            MsgBox("SS: " & ex.Message)
            DoLog("SS: " & ex.Message)
        End Try
    End Function
    Function SendMail()
        Try
            Dim Smtp_Server As New SmtpClient
            Dim e_mail As New MailMessage()
            Smtp_Server.UseDefaultCredentials = False
            Smtp_Server.Credentials = New Net.NetworkCredential(stremail, strepass)
            Smtp_Server.Port = 587 'port
            Smtp_Server.EnableSsl = True
            Smtp_Server.Host = hostname

            e_mail = New MailMessage()
            e_mail.From = New MailAddress(stremail)
            e_mail.To.Add(strsendto)
            e_mail.Subject = subject
            e_mail.IsBodyHtml = False
            e_mail.Body = strtextbody

            If HaveAttachment = True Then e_mail.Attachments.Add(att)



            Smtp_Server.Send(e_mail)
        Catch ex As Exception
            MsgBox("SM: " & ex.Message)
            DoLog("SM: " & ex.Message)
            DoLog("Ending Program")
            Application.Exit()

        End Try
    End Function
    Function DoLog(ByVal Line As String)
        If My.Computer.FileSystem.FileExists(Format(Date.Now, "ddMMyy") & ".log") = False Then
            Dim writer As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(Format(Date.Now, "ddMMyy") & ".log", True)
            writer.Close()
        End If
        Try
            Dim writer2 As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(Format(Date.Now, "ddMMyy") & ".log", True)
            writer2.WriteLine("[" & Date.Now & "] : " & Line)
            writer2.Close()
        Catch ex As Exception
            MsgBox("Can't start logging.")
        End Try
    End Function
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Timer1.Stop()
        Try

            Dim cn As New ADODB.Connection()
            Dim rs As New ADODB.Recordset()

            cn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & DatabasePath & ";"
            cn.Mode = ADODB.ConnectModeEnum.adModeShareDenyNone
            cn.Open()
            DoLog("Connecting to Email Data")
            rs.Open("SELECT * FROM email_data WHERE Sent=0", cn)
            If rs.EOF = False Then
                strsendto = rs.Fields.Item(1).Value.ToString
                subject = rs.Fields.Item(2).Value.ToString
                strtextbody = rs.Fields.Item(3).Value.ToString
                If My.Computer.FileSystem.FileExists(rs.Fields.Item(4).Value.ToString) = True Then
                    att = New System.Net.Mail.Attachment(rs.Fields.Item(4).Value.ToString)
                    HaveAttachment = True
                Else
                    HaveAttachment = False
                End If
                Call SendMail()
                DoLog("Updating Record")
                cn.Execute("UPDATE email_data SET sent=-1 WHERE ID = " & rs.Fields.Item(0).Value.ToString)
            End If
            DoLog("Disconnecting to Email Data")
            rs.Close()
            rs.ActiveConnection = Nothing
            cn.Close()

        Catch ex As Exception
            MsgBox("TT: " & ex.Message)
            DoLog("TT: " & ex.Message)
            DoLog("Ending Program")
            Application.Exit()

        End Try
        Me.Timer1.Start()

    End Sub
End Class